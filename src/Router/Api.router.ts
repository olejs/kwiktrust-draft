import {MainRouter} from "./Main.router";

import Application from "koa";
import {SequelizeModel} from "../Models/index";
const bodyParse=require('koa-parser');
const cors=require('@koa/cors');

import Router from "koa-router";
import {AuthenticationRouter} from "../Controllers/Authenication/index.route";
import {Context} from "koa";
import {UploadRouter} from '../Controllers/Upload/index.route';
import {DocumentsRouter} from '../Controllers/Documents/index.route';


export class ApiRouter extends MainRouter{
    private router:any;
    private prefix:string='api';
    private authenticationRouter:AuthenticationRouter;
    private uploadFileRouter:UploadRouter;
    private documentsRouter:DocumentsRouter;

    constructor(){
        super();
        this.authenticationRouter=new AuthenticationRouter();
        this.uploadFileRouter=new UploadRouter();
        this.documentsRouter=new DocumentsRouter();
    }

    /**
     * Authentication router
     */
    private authRouter(){
        this.use(bodyParse());
        this.use(cors({origin:'*'}));

        this.router.post(`/${this.prefix}/auth/login`,async (ctx:Context)=>this.authenticationRouter.login(ctx));
        this.router.post(`/${this.prefix}/auth/signup`,async (ctx:Context)=>this.authenticationRouter.register(ctx));
        this.router.post(`/${this.prefix}/auth/signup/verify`,async (ctx:Context)=>this.authenticationRouter.confirmRegister(ctx));
/*        this.router.post(`/${this.prefix}/auth/send-verification`,ForgotPasswordController.setup());
        this.router.post(`/${this.prefix}/auth/change-password`,PasswordChangeController.setup());*/
    }

    private uploadRouter() {
        this.use(bodyParse());
        this.use(cors({origin:'*'}));

        this.router.post(`/${this.prefix}/upload`, async (ctx:Context) => this.uploadFileRouter.upload(ctx));
        this.router.get(`/${this.prefix}/upload`, async (ctx:Context) => this.uploadFileRouter.upload(ctx));
        this.router.get(`/example`, async (ctx:Context) => this.documentsRouter.get(ctx));
    }


    /**
     * setup router
     */
    public boostrap(){

        this.router=new Router();

        this.authRouter();
        this.uploadRouter();
       new SequelizeModel();
        this.use(this.router.routes());
    }


    get app():Application{
        return this.koa;
    }

}
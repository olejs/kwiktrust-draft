import Application from "koa";
import {Middleware} from "koa";


export class MainRouter{
    private _koa:Application;
    constructor(){
        this._koa=new Application();

    }


    get koa(): Application {
        return this._koa;
    }


    protected use(middleWare:Middleware){
        this._koa.use(middleWare)
    }
}
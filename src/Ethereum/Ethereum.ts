const Web3 = require('web3');
import {appConfig} from '../config/app.config';
import {UserModel} from '../Models/user.model';

export class Ethereum {

    public _web3:any;
    public _contract:any;

    constructor(){
        this._web3 = new Web3(appConfig.testnetAddr);
        const account = this._web3.eth.accounts.privateKeyToAccount(appConfig.privateKey);
        this._web3.eth.accounts.wallet.add(account);
    }

    get web3() {
        return this._web3;
    }

    set web3(value) {
        this._web3 = value;
    }
}
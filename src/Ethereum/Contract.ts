import {Ethereum} from './Ethereum';
import {appConfig} from '../config/app.config.js';
import {kwikTrustAbi} from './smartContracts/kwiktrust.abi';
import toInt = require('validator/lib/toInt');

const Web3 = require('web3');

export class Contract extends Ethereum {
    public contract:any;

    constructor() {
        super();
        this.contract = this._web3.eth.Contract(kwikTrustAbi, appConfig.smartContractHash);
    }

    async gasEstimate(userHash:any, signature:any, initializationTime:any, expirationTime:any, id:any) {
        return await this.contract.methods
            .add(userHash, this.web3.utils.utf8ToHex(signature), initializationTime, expirationTime, id)
            .estimateGas({
                from: this.web3.eth.accounts.wallet[0].address
            });
    }

    async uploadDocument(userHash:any, signature:any, initializationTime:any, expirationTime:any, id:any) {

        const from = this.web3.eth.accounts.wallet[0].address;
        console.log(from);
        const nonce = await this.web3.eth.getTransactionCount(from, "pending");
        console.log(nonce);
        const gas = await this.gasEstimate(userHash, signature, initializationTime, expirationTime, id);
        console.log(gas);
        this.contract.methods.add(userHash, this.web3.utils.utf8ToHex(signature), initializationTime, expirationTime, id).send({
            gasPrice: gas * 2,
            gas: gas * 10,
            from,
            nonce
        });
    }

    async getUserDocuments(userHash:any) {
        const docData =  await this.contract.methods.get(userHash).call();
        return docData;
    }
}
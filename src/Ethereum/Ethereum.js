"use strict";
exports.__esModule = true;
var Web3 = require('web3');
var app_config_1 = require("../config/app.config");
var Ethereum = /** @class */ (function () {
    function Ethereum() {
        this._web3 = new Web3(app_config_1.appConfig.testnetAddr);
        var account = this._web3.eth.accounts.privateKeyToAccount(app_config_1.appConfig.privateKey);
        this._web3.eth.accounts.wallet.add(account);
    }
    Object.defineProperty(Ethereum.prototype, "web3", {
        get: function () {
            return this._web3;
        },
        set: function (value) {
            this._web3 = value;
        },
        enumerable: true,
        configurable: true
    });
    return Ethereum;
}());
exports.Ethereum = Ethereum;

import {Ethereum} from './Ethereum';
import {UserModel} from '../Models/user.model.js';
import {appConfig} from '../config/app.config.js';
let Web3 = require('web3');

export class Account extends Ethereum{

    constructor() {
        super();
    }

    static async create() {
        let ethInstance = new Web3(appConfig.testnetAddr);
        return ethInstance._web3.eth.accounts.create();
    }

    public async getBalance(user: UserModel) {
        return this._web3.eth.getBalance(user.public_key, function (res:any) {
            console.log(res);
        });
    }

}
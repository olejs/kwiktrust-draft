import {Context} from 'koa';
import {UploadFileController} from './UploadFile.controller';

export class UploadRouter {

    private _upload_file:UploadFileController;

    constructor(){

        this._upload_file=new UploadFileController();
    }

    public async upload(ctx:Context) {

        this._upload_file._context=ctx;
        return await this._upload_file.setup();
    }
}
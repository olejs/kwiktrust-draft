import {Context} from 'koa';
import {DocumentsController} from './Documents.controller';

export class DocumentsRouter {

    private _documents:DocumentsController;

    constructor(){
        this._documents=new DocumentsController();
    }

    public async get(ctx:Context) {
        this._documents._context=ctx;
        return await this._documents.setup();
    }
}
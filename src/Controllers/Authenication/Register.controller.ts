import {AuthenticationController} from "./Authentication.controller";
import {Context} from "koa";
import {UserModel} from "../../Models/user.model";
import {RequestService} from "../../Services/Request.services";
import {BlackListModel} from "../../Models/blacklist.model";
import {appConfig} from "../../config/app.config";
import {HashService} from "../../Services/Hash.services";
import {TokenService} from "../../Services/Token.services";
import {Account} from '../../Ethereum/Account';
export class RegisterController extends AuthenticationController{

    constructor(){
        super();
    }

    public async setup(){
        await  this.register();
    }


    public async confirmRegister() {
        this.body = this._context.request.body;
        if (!this.body || !this.body.token) {
            this.response.statusCode = 400;
            this.response.statusText = 'Token is missing';
            this._context.body=this.response.response();
            return;
        }

        try{
        let tokenService = new TokenService();
        let token:any = await tokenService.verify(this.body.token)

        if (!token.id || !token.email || token.type != 'registration') {
            this.response.statusCode = 400;
            this.response.statusText = 'Verification failed. Invalid token!';
            this.response.response();
            this._context.body=this.response.response();
            return;
        }

        let user = await UserModel.findOne({
            where:{
                email: token.email
            }
        });

        if (!user || !user.id) {
            this.response.statusCode = 400;
            this.response.statusText = 'User does not exists or Invalid verification code!';
            this._context.body=this.response.response();
            return;
        }

        user.status = 1;
        await user.save();

        this.response.statusCode = 200;
        this.response.statusText = 'Your email confirmation is successful';
            this._context.body=this.response.response();

    }catch(e) {
            this.response.statusCode = 500;
            this.response.statusText = 'Error occurred';
            this._context.body=this.response.response();
        }
    }
    private async register(){
        this.body=this._context.request.body;

        let missingBody:string | undefined=RequestService.isMissing(this.body,['firstname','lastname','email','password']);
        if(missingBody){
            this.response.statusCode=400;
            this.response.statusText='Some fields are missing';
            let missing={} as any;
            missing[missingBody as string]=missingBody+" is missing."
            this.response.data=missing;
            this._context.body=this.response.response();
            return;
        }
        try{

            //check if email already exists
            let user=await UserModel.findOne({
                where:{
                    email:this.body.email

                }
            });


            if(user && user.id){

                this.response.statusCode=400;
                this.response.statusText='User already exists';
                this.response.data={
                    email:'User already exists'
                }
                this._context.body=this.response.response();
                return;
            }


            /**
             * check if we blacklisted user
             */

            let blacklist=await BlackListModel.findOne({where:{
                email:this.body.email
            }});


            // if(blacklist && blacklist.id){
            //     this.response.statusText='You do not have permission to register';
            //     this.response.statusCode=403;
            //     this._context.body=this.response.response();
            //     return;
            // }


            /**
             * invalid email suffix - blacklist
             */
            // if(!this.body.email.endsWith(appConfig.emailPrefix)){
            //     let blackList=await BlackListModel.create({
            //         email:this.body.email
            //     });
            //     this.response.statusText='You do not have permission to register';
            //     this.response.statusCode=403;
            //     this._context.body=this.response.response();
            //     return;
            // }


            let hashService=new HashService();
            hashService.plainPassword=this.body.password;
            this.body.password=await hashService.hashPassword();
            // let account = new Account('raz dwa');
            // let blockchainSeed = await ac
            // this.body.private_key = blockchainSeed.privateKey;
            // this.body.public_key = blockchainSeed.address;
            this.body.status=2;
            this.createdUser=await UserModel.create(this.body);

            let mailer=await this.sendVerificationMsg();

            if(this.createdUser && this.createdUser.id && mailer) {
                this.response.statusCode = 201;
                this.response.statusText = 'Registration was successful!';
                this.response.data={id:this.createdUser.id};
                this._context.body = this.response.response();
                return
            }
            this.response.statusCode=400;
            this.response.statusText='Registration failed';
            this._context.body=this.response.response();
        }catch(e){
            console.log(e);
            this.response.statusText='Error occurred';
            this.response.statusCode=500;
            this._context.body=this.response.response()
        }

    }




    set context(value:Context){
        this._context=value;
    }
}
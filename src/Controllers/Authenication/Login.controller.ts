import {AuthenticationController} from "./Authentication.controller";
import {Context} from "koa";
import {RequestService} from "../../Services/Request.services";
import {UserModel} from "../../Models/user.model";
import {BlackListModel} from "../../Models/blacklist.model";
import {appConfig} from "../../config/app.config";
import {HashService} from "../../Services/Hash.services";
import {TokenService} from "../../Services/Token.services";
export class LoginController extends AuthenticationController{
    constructor(){
        super();
    }
    public async setup(){
        await this.login();
    }

    private async login(){
        const requestBody=this._context.request.body;
        try{

            const missingParameter=RequestService.isMissing(requestBody,['email','password']);

            if(missingParameter){

                this.response.statusText='Some fields are missing';
                let missing={} as any;
                missing[missingParameter as string]=missingParameter+" is missing.";
                this.response.data=missing;
                this._context.body=this.response.response();
                return;

            }

            /**
             * check if we blacklisted user
             */

            // let blacklist=await BlackListModel.findOne({where:{
            //     email:requestBody.email
            // }});
            //
            // if(blacklist && blacklist.id){
            //     this.response.statusText='You do not have permission to register';
            //     this.response.statusCode=403;
            //     this.response.data={
            //         email:'Email is invalid',
            //         password:'Password is invalid'
            //     }
            //     this._context.body=this.response.response();
            //     return;
            // }


            /**
             * blacklist user without the server host
             */

            // if(!requestBody.email.endsWith(appConfig.emailPrefix)){
            //     await new BlackListModel({
            //         email:requestBody.email
            //     });
            //     this.response.statusCode=403;
            //
            //     this.response.statusText='You do not have permission to register';
            //     this.response.data={
            //         email:'Email is invalid',
            //         password:'Password is invalid'
            //     }
            //     this._context.body=this.response.response();
            //     return;
            // }


            let user=await UserModel.findOne({
                where:{
                    email:requestBody.email
                }
            });
            // console.log(requestBody.email);
            console.log(user);


            if(!user || !user.id){
                this.response.statusCode=400;
                this.response.statusText='Invalid login!';
                this.response.data={
                    email:'Email does not exists!',
                    password:'Password not valid'
                };
                this._context.body=this.response.response();
                return;
            }

            if(user && user.status==2){
                this.response.statusCode=400;
                this.response.statusText='Please verify your account!';
                this.response.data={
                    email:'Verification is required!'
                };
                this.body={
                    firstname:user.firstname,
                    lastname:user.lastname
                };
                this.createdUser=user;
                await this.sendVerificationMsg();
                this._context.body=this.response.response();
                return;
            }


            let hashService=new HashService();
            hashService.plainPassword=requestBody.password;
            hashService.hashedPassword=user.password;
            const isMatch=await hashService.verifyPassword();
            if(!isMatch){
                this.response.statusCode=400;
                this.response.statusText='Invalid login!';
                this.response.data={
                    email:'Email exists',
                    password:'Password not valid'
                };
                this._context.body=this.response.response();
                return;
            }

            const tokenService=new TokenService();
            tokenService.duration=24;
            this.response.statusCode=200;
            this.response.statusText='Login successful!';
            this.response.data={
                token:await tokenService.sign({id:user.id,email:user.email,type:'auth'}),
                expires:tokenService.duration
            }

            this._context.body=this.response.response();

        } catch(e) {
            console.log('siemka');
            console.log(e);
            this.response.statusCode=400;
            this.response.statusText='Error occurred';
            this.response.data={
                email:'Email is invalid',
                password:'Password is invalid'
            }
            this._context.body=this.response.response();
        }

    }

    set context(value:Context){
        this._context=value;
    }
}
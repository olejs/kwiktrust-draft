import {Context} from "koa";
import {RequestService} from "../../Services/Request.services";
import {UserModel} from "../../Models/user.model";
import {appConfig} from "../../config/app.config";
import {TokenService} from "../../Services/Token.services";
import {MailerService} from "../../Services/Mailer.services";

export class AuthenticationController{
    private _response:RequestService;
    protected _context:any;
    protected createdUser:UserModel;
    protected body:any;
    constructor(){
        this._response=new RequestService(this._context);
    }



    protected async sendVerificationMsg(){
        console.log(this.createdUser);
        console.log(this.body);
        // if(!this.createdUser || !this.createdUser.id || !this.body || !this.body.email){
        //     throw new Error('Registration failed. User was not created');
        // }
        let mailer=new MailerService();
        mailer.to=this.body.email;
        mailer.subject=appConfig.projectName+' Registration';
        let tokenService=new TokenService();

        tokenService.duration=(1/2);
        const signed=await tokenService.sign({id:this.createdUser.id,email:this.createdUser.email,type:'registration'});

        mailer.html=`Hi ${this.createdUser.firstname} <br/> Thank you for registering with us and welcome to ${appConfig.projectName}. 
        Here is your <a href="${appConfig.url}?t=${signed}">Confirmation link</a> to complete your registration<br/>Thank you.`;
        // return await mailer.mail();
        return true
    }

    get response(): RequestService {
        return this._response;
    }
}
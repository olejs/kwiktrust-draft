import {LoginController} from "./Login.controller";
import {RegisterController} from "./Register.controller";
import {Context} from "koa";
export class AuthenticationRouter{
    private _login:LoginController;
    private _register:RegisterController;
    constructor(){

        this._login=new LoginController();
        this._register=new RegisterController();

    }

    public async  login(ctx:Context){
        this._login.context=ctx;
        return await this._login.setup();
    }

    public async register(ctx:Context) {
        this._register.context=ctx;
        return await this._register.setup();
    }

    public async confirmRegister(ctx:Context){
        this._register.context=ctx;
        return await this._register.confirmRegister();
    }
}
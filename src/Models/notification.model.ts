

import {UserModel} from "./user.model";
import { DataTypes, Model, Sequelize} from "sequelize";
import {CompanyModel} from "./company.model";
export class NotificationModel extends Model {
    public id:number;
    public name:string;
    public description:string;
    public company:number;
    public readonly createdAt:Date;
    public readonly updatedAt:Date;
    public readonly deletedAt:Date;
    public user:number;


    public static setup(sequelize:Sequelize){
        NotificationModel.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            name: {
                type: DataTypes.STRING
            },
            description: {
                type: DataTypes.STRING(1000)
            },
            company: {
                type: DataTypes.INTEGER.UNSIGNED
            },
            user: {
                type: DataTypes.INTEGER.UNSIGNED
            }
        },{
            sequelize,
            tableName:'Notifications'
        })

    }

    public static association(){

        NotificationModel.belongsTo(CompanyModel,{
            foreignKey:'company'
        });
        NotificationModel.belongsTo(UserModel,{
            foreignKey:'user'
        })
    }
}

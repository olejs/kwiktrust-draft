"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var user_model_1 = require("./user.model");
var sequelize_1 = require("sequelize");
var company_model_1 = require("./company.model");
var NotificationModel = /** @class */ (function (_super) {
    __extends(NotificationModel, _super);
    function NotificationModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NotificationModel.setup = function (sequelize) {
        NotificationModel.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            name: {
                type: sequelize_1.DataTypes.STRING
            },
            description: {
                type: sequelize_1.DataTypes.STRING(1000)
            },
            company: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED
            },
            user: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED
            }
        }, {
            sequelize: sequelize,
            tableName: 'Notifications'
        });
    };
    NotificationModel.association = function () {
        NotificationModel.belongsTo(company_model_1.CompanyModel, {
            foreignKey: 'company'
        });
        NotificationModel.belongsTo(user_model_1.UserModel, {
            foreignKey: 'user'
        });
    };
    return NotificationModel;
}(sequelize_1.Model));
exports.NotificationModel = NotificationModel;

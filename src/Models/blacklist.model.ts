import {DataTypes, Model, Sequelize} from "sequelize";
export class BlackListModel extends Model{
    public id:number;
    public email:number;
    public readonly  createdAt?:Date;
    public readonly  updatedAt?:Date;
    public readonly deletedAt?:Date;

    public static setup(sequelize:Sequelize){
        BlackListModel.init({
            id:{
                type:DataTypes.INTEGER.UNSIGNED,
                primaryKey:true,
                autoIncrement:true
            },
            email:{
                type:DataTypes.STRING
            }
        },{sequelize,
        tableName:'blacklisted'})
    }
}
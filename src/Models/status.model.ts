
import {DataTypes, Model, Sequelize} from "sequelize";
import {UserModel} from "./user.model";
export class StatusModel extends Model {

    constructor() {
        super();
    }

    public id:number;
    public name:string;

    public static setup(sequelize:Sequelize){
            StatusModel.init({
                id: {
                    type: DataTypes.INTEGER.UNSIGNED,
                    primaryKey: true,
                    autoIncrement: true
                },
                name: {
                    type: DataTypes.STRING
                }
            }, {
                sequelize,
                tableName: 'Statuses'
            });
    }

    public static association(){
        StatusModel.hasMany(UserModel,{foreignKey:'status'})
    }
}
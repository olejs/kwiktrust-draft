"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var sequelize_1 = require("sequelize");
var user_model_1 = require("./user.model");
var StatusModel = /** @class */ (function (_super) {
    __extends(StatusModel, _super);
    function StatusModel() {
        return _super.call(this) || this;
    }
    StatusModel.setup = function (sequelize) {
        StatusModel.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: sequelize_1.DataTypes.STRING
            }
        }, {
            sequelize: sequelize,
            tableName: 'Statuses'
        });
    };
    StatusModel.association = function () {
        StatusModel.hasMany(user_model_1.UserModel, { foreignKey: 'status' });
    };
    return StatusModel;
}(sequelize_1.Model));
exports.StatusModel = StatusModel;

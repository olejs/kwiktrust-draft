


import {DataTypes, Model, Sequelize} from "sequelize";
import {NotificationModel} from "./notification.model";
export class CompanyModel extends Model{
    public id:number;
    public name:string;
    public website:string;
    public contactName:string;
    public email:string;
    public phone:string;
    public address:string;
    public readonly createdAt?:Date;
    public readonly updatedAt?:Date;
    public readonly deletedAt?:Date;

    public static setup(sequelize:Sequelize){
        CompanyModel.init({
            id:{
                type:DataTypes.INTEGER.UNSIGNED,
                primaryKey:true,
                autoIncrement:true
            },
            name:{
                type:DataTypes.STRING
            },
            website:{
                type:DataTypes.STRING

            },
            contactName:{
                type:DataTypes.STRING

            },
            email:{
                type:DataTypes.STRING

            },
            phone:{
                type:DataTypes.STRING

            },
            address:{
                type:DataTypes.STRING
            }
        },{
            sequelize,
            tableName:'companies'
        });
    }

    public static association(){
        CompanyModel.hasMany(NotificationModel,{
            foreignKey:'company'
        })
    }
}

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var sequelize_1 = require("sequelize");
var notification_model_1 = require("./notification.model");
var CompanyModel = /** @class */ (function (_super) {
    __extends(CompanyModel, _super);
    function CompanyModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CompanyModel.setup = function (sequelize) {
        CompanyModel.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: sequelize_1.DataTypes.STRING
            },
            website: {
                type: sequelize_1.DataTypes.STRING
            },
            contactName: {
                type: sequelize_1.DataTypes.STRING
            },
            email: {
                type: sequelize_1.DataTypes.STRING
            },
            phone: {
                type: sequelize_1.DataTypes.STRING
            },
            address: {
                type: sequelize_1.DataTypes.STRING
            }
        }, {
            sequelize: sequelize,
            tableName: 'companies'
        });
    };
    CompanyModel.association = function () {
        CompanyModel.hasMany(notification_model_1.NotificationModel, {
            foreignKey: 'company'
        });
    };
    return CompanyModel;
}(sequelize_1.Model));
exports.CompanyModel = CompanyModel;

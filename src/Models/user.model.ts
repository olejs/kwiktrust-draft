
import {DataTypes, Model, Sequelize} from "sequelize";
import {CompanyModel} from "./company.model";
import {NotificationModel} from "./notification.model";
import {StatusModel} from "./status.model";
export class UserModel extends Model {
    constructor() {
        super();
    }
    public id:number;
    public firstname:string;
    public lastname:string;
    public password:string;
    public email:string;
    public phone:string;
    public status:number;
    public public_key:string;
    public private_key:string;
    public readonly createdAt?:Date;
    public readonly updatedAt?:Date;
    public readonly deletedAt?:Date;

    public static setup(sequelize:Sequelize){

        UserModel.init({
            id:{
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
            },
            firstname:{
                type:DataTypes.STRING
            },
            lastname:{
                type:DataTypes.STRING
            },
            password:{
                type:DataTypes.STRING
            },
            email:{
                type:DataTypes.STRING
            },
            status:{
                type:DataTypes.INTEGER.UNSIGNED
            },
            phone:{
                type:DataTypes.STRING
            }
        },{
            sequelize,
            tableName:'Users'
        });
    }


    public static association(){

        UserModel.hasMany(NotificationModel,{
            foreignKey:'user'
        });

        UserModel.belongsTo(StatusModel,{
            foreignKey:'status'
        })

    }
}
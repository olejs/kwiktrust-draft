"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var sequelize_1 = require("sequelize");
var notification_model_1 = require("./notification.model");
var status_model_1 = require("./status.model");
var UserModel = /** @class */ (function (_super) {
    __extends(UserModel, _super);
    function UserModel() {
        return _super.call(this) || this;
    }
    UserModel.setup = function (sequelize) {
        UserModel.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            firstname: {
                type: sequelize_1.DataTypes.STRING
            },
            lastname: {
                type: sequelize_1.DataTypes.STRING
            },
            password: {
                type: sequelize_1.DataTypes.STRING
            },
            email: {
                type: sequelize_1.DataTypes.STRING
            },
            status: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED
            },
            phone: {
                type: sequelize_1.DataTypes.STRING
            }
        }, {
            sequelize: sequelize,
            tableName: 'Users'
        });
    };
    UserModel.association = function () {
        UserModel.hasMany(notification_model_1.NotificationModel, {
            foreignKey: 'user'
        });
        UserModel.belongsTo(status_model_1.StatusModel, {
            foreignKey: 'status'
        });
    };
    return UserModel;
}(sequelize_1.Model));
exports.UserModel = UserModel;

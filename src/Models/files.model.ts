import {NotificationModel} from './notification.model.js';
import {StatusModel} from './status.model.js';
import {DataTypes, Model, Sequelize} from 'sequelize';
import {UserModel} from './user.model.js';

export class FilesModel extends Model {
    public id: number;
    public name: string;
    public label: string;
    public signature: string;
    public extension: string;
    public user_id: string;

    public readonly createdAt?: Date;
    public readonly updatedAt?: Date;
    public readonly deletedAt?: Date;


    public static setup(sequelize: Sequelize) {
        FilesModel.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING
            },
            label: {
                type: DataTypes.STRING
            },
            signature: {
                type: DataTypes.STRING
            },
            extension: {
                type: DataTypes.STRING
            },
            user_id: {
                type: DataTypes.STRING
            },
        }, {
            sequelize,
            tableName: 'files'
        });
    }


    public static association() {
        UserModel.belongsTo(UserModel, {
            foreignKey: 'user_id'
        })
    }
}
///<reference path="status.model.ts"/>


import {UserModel} from "./user.model";
import { Sequelize} from "sequelize";
import {dbConfig} from "../config/database";
import {NotificationModel} from "./notification.model";
import {CompanyModel} from "./company.model";
import {BlackListModel} from "./blacklist.model";
import {StatusModel} from "./status.model";
const sequelize = new Sequelize(`${dbConfig.dialet}://${dbConfig.username}:${dbConfig.password}@${dbConfig.host}:${dbConfig.port}/${dbConfig.databaseName}`);
import {appConfig} from '../config/app.config';


export class SequelizeModel{
    constructor(){
      this.assoc();
      sequelize.sync({force:true}).then(() => this.fill());
    }

    private assoc(){

        UserModel.setup(sequelize);
        NotificationModel.setup(sequelize);
        CompanyModel.setup(sequelize);
        BlackListModel.setup(sequelize);
        StatusModel.setup(sequelize);

        StatusModel.association();
        UserModel.association();
        CompanyModel.association();
        NotificationModel.association();
    }

    private fill() {

    }
}

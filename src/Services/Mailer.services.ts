
import nodemailer from 'nodemailer'

import {appConfig} from "../config/app.config";
export class MailerService{

    private _to:string;
    private _text:string;
    private _html:string;
    private _service='1and1';
    private _subject:string;
    private _attachments:Array<{filename:string,content:any}>;
    constructor(){

    }


    public async mail(){
        const transport=nodemailer.createTransport(appConfig.email)
        return await transport.sendMail(this.settings())
    }


    private settings(){
        return {
            service:this._service || '1and1',
            requireTLS:false,
            ignoreTLS:true,
            secure: false,
            attachments:this._attachments,
            from: appConfig.email.auth.user, // sender address
            to: this._to, // list of receivers
            subject: this._subject, // Subject line
            text: this._text, // plain text body
            html: this._html // html body
        };
    }

    set to(value: string) {
        this._to = value;
    }

    set text(value: string) {
        this._text = value;
    }

    set html(value: string) {
        this._html = value;
    }

    get attachments(): Array<{ filename: string; content: any }> {
        return this._attachments;
    }

    set service(value: string) {
        this._service = value;
    }

    set subject(value: string) {
        this._subject = value;
    }


}
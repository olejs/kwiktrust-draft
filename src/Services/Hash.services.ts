import bcrypt from 'bcrypt'


export class HashService{
    private _plainPassword:string;
    private _hashedPassword:string;
    constructor(){

    }


    public async hashPassword():Promise<any> {
        if(!this._plainPassword || !this._plainPassword.length){
            throw new Error('Plain and hash password are required')
        }

        let salt=await bcrypt.genSalt(10);
        return await bcrypt.hash(this._plainPassword,salt);

    }


    public async verifyPassword(){
        if(!this._plainPassword || !this._plainPassword.length || !this._hashedPassword|| !this._hashedPassword.length){
            throw new Error('Plain and hash password are required')
        }

        return await bcrypt.compare(this._plainPassword,this._hashedPassword)
    }

    set plainPassword(value: string) {
        this._plainPassword = value;
    }

    set hashedPassword(value: string) {
        this._hashedPassword = value;
    }
}
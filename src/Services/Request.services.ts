import {Context} from "koa";
export class RequestService{
    private _statusCode:number=200;
    private _statusText:string='';
    private _data:any={};
    constructor(public ctx:Context){

    }


    public response(){
        return {
            statusCode:this._statusCode,
            statusText:this._statusText,
            data:this._data
        }
    }


    public static isMissing(requestBody:any,requiredParam:Array<string>){
        for(let i in requiredParam){
            if(!requestBody[requiredParam[i]] && requestBody[requiredParam[i]]!==0 && requestBody[requiredParam[i]]!==false){
                return requiredParam[i];
            }
        }
    }


    public static removeFake(requestBody:any,canContain:Array<string>){
        let updateObject={} as any;
        for(let i in canContain){
            if((requestBody[canContain[i]] || requestBody[canContain[i]]===0 || requestBody[canContain[i]]===false) && requestBody[canContain[i]]!==null){
                updateObject[canContain[i]]=requestBody[canContain[i]];
            }
        }
        return updateObject;
    }

    public static random(length?:number){
        return Math.random().toString(length || 36).substr(2,8);
    }

    set statusCode(value: number) {
        this._data={};
        this._statusCode = value;
    }

    set statusText(value: string) {
        this._statusText = value;
    }

    set data(value: any) {
        this._data = value;
    }
}
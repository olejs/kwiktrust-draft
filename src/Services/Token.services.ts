import Jwt from 'jsonwebtoken';
import {appConfig} from "../config/app.config";

export class TokenService{
    private _duration:number=1; //hour
    constructor(){

    }


    public sign(data:any){
        return new Promise((resolve,reject)=>{
            Jwt.sign(data,appConfig.tokenSalt,{expiresIn:(this._duration | 24)*3600},(error, data)=>{
                if(error)
                    reject(error);
                resolve(data);
            });

        })
    }


    public verify(token:string){
        return new Promise((resolve,reject)=>{
            Jwt.verify(token,appConfig.tokenSalt,(err, decoded)=>{
                if(decoded){
                    resolve(decoded);
                }
                if(err){
                    reject(err);
                }
            });
        })
    }

    get duration(): number {
        return this._duration * 3600;
    }

    set duration(value: number) {
        this._duration = value;
    }
}